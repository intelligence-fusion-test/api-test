# Test Requirements

This application is a reduced version of our API and contains a JSON API with two endpoints:

- POST /incidents
- GET /incidents/:id

It has intentionally been developed in a manner that develops away from our core development principles and beliefs (see [principles](principles.md)) and requires improvement. Both endpoints are functional although brittle in nature. HTTP response codes do not always match the error message returned and there are violations of the single responsibility principle throughout. You are required to complete the following:

### Task 1
We have intentionally been inconsistent in our approach and require you to improve the current functionality of both the POST and GET endpoints to be inline with the development philosophy and architecture outlined in [principles](principles.md) and [architecture](architecture.md). You can also use this chance to correct any mistakes you find.

### Task 2
Since being implemented it has been noted that the `Incident` entity is missing a required property called involved party. The required schema for an involved party is:
```json
"involvedParty": {
  "id": "ac6c736d-d1dd-4ec7-9661-43a3322cde3c",
  "name": "President of United States",
  "involvement": "Directly Targeted"
}
```

You are required to update both endpoints to include this required property including the persistence and retrieval from the database.

### Notes
- Examples incidents can be found in [incidents.json](../database/data/incidents.json) to assist you with the initial creation of incidents via the POST endpoint and help outline the expected response schema from the GET endpoint. 

- All authentication and authorization has been removed for ease of use and are not a requirement of this assignment.

- This application currently contains one test in the format we write our tests.

- Follow the setup instructions on the [Getting Started](getting_started.md) in order to gain a working environment locally.

- Once the docker services are built the API is accessible via [http://localhost:8001](http://localhost:8001).

- You should only need to work inside the "app", "database" and "tests" folders.

### What we are looking for
- An understanding of our architecture and core principles with the ability to convert this understanding into code.

- Attention to detail - there are deliberate mistakes within this codebase that violate our coding standards that need addressing.

- Tests to back up your implementation.
