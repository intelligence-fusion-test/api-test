## Getting Started
A recent version of Docker and Docker Compose is required for this project to run. 

### Services
This test application contains three services:

- Web - Nginx reverse proxy directing traffic to the application service
- Application - PHP service containing the application code
- Database - Postgres storage engine

### Running the Services
Navigate to the root of the project and run:
```bash
docker-compose up -d --build
```

If this is the first time this has been run, the project image can take some time to build.

On first run, execute the following command from the root of the project. This will install the required PHP dependencies and execute the required migrations:
```bash
docker-compose exec application composer setup
```

If you need to exec into the running PHP application container:
```bash
docker-compose exec application sh
```

To stop running the system, run:
```bash
docker-compose down -v
```

## Running the Tests
Test scripts have been included in the application image. When the system is running, run the following command from the root of the project:
```bash
docker-compose run --rm application test
```
