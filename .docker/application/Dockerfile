### Prepare the base environment =======================================================================================
FROM php:7.3.5-fpm-alpine AS base

LABEL description="API server for Intelligence Fusion 2"

# Allow the application version to be set at build time
ARG APP_VERSION

# Prepare application variables
ENV APP_VERSION ${APP_VERSION}
ENV APP_DIR /srv/fusion
ENV PATH ${APP_DIR}:${APP_DIR}/vendor/bin:${PATH}
ENV APP_TIMEZONE UTC

# Prepare cache and queue variables
ENV CACHE_DRIVER file
ENV QUEUE_DRIVER sync

# Add custom config files and scripts
COPY .docker/application/php-production.ini /usr/local/etc/php/php.ini
COPY .docker/application/fpm-www.conf /usr/local/etc/php-fpm.d/www.conf
COPY .docker/application/cron.txt /var/spool/cron/crontabs/root
COPY .docker/application/docker-php-* /usr/local/bin/

# Install libraries and extensions
RUN docker-php-configure && ln -s docker-php-entrypoint /usr/local/bin/fusion
RUN docker-php-ext-install exif && docker-php-ext-enable exif

# Install nodejs for controlling puppeteer PDF service
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true
RUN apk add --update npm && npm install --global --unsafe-perm puppeteer@chrome-77

# Set the working directory to the root of the application and the entrypoint
WORKDIR ${APP_DIR}

# Set the entrypoint to the custom "inscript" and use "serve" as the default command
ENTRYPOINT ["fusion"]
CMD ["serve"]


### Prepare the final production image =================================================================================
FROM base AS production

# Prepare environment variables
ENV APP_ENV production

# Copy in the application files
COPY . ${APP_DIR}

# Set up the storage directory and install dependencies
RUN mkdir -p \
        storage/app \
        storage/framework/cache/data \
        storage/framework/sessions \
        storage/framework/testing \
        storage/framework/views \
        storage/logs \
    && chmod -R 0755 storage \
    && chown -R www-data:www-data ${APP_DIR} \
    && docker-php-composer-install \
    && composer dump-autoload \
    && composer install --no-interaction --no-suggest --no-dev \
    && rm -rf ~/.composer $(which composer)

# Put the storage directory in a volume
VOLUME ${APP_DIR}/storage

# Install and enable OpCache
COPY .docker/application/php-opcache.ini php-opcache.ini
RUN docker-php-opcache-install


### Add development and testing tools ==================================================================================
FROM production AS development

# Prepare environment variables
ENV APP_ENV development
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV XDEBUG_VERSION 2.7.2

# Install composer, fetch development dependencies, install XDebug and disable OPcache
RUN docker-php-composer-install \
    && composer dump-autoload \
    && composer install --no-interaction --no-suggest \
    && docker-php-xdebug-install \
    && rm /usr/local/etc/php/conf.d/docker-php-ext-opcache.ini
