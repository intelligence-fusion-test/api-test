#!/usr/bin/env sh

function serveApplication() {
    docker-php-await ${DB_HOST}:${DB_PORT} -- php-fpm "$@"
}

function healthCheckApplication() {
    if pgrep php-fpm > /dev/null; then
        SCRIPT_NAME=index.php \
        SCRIPT_FILENAME=/srv/fusion/public/index.php \
        REQUEST_METHOD=GET \
        cgi-fcgi -bind -connect 127.0.0.1:9000/health || exit 1;
    else
        exit 1;
    fi
}

function runComposer() {
    composer "$@"
}

function runMigrate() {
    docker-php-await ${DB_HOST}:${DB_PORT} -- artisan migrate
}

function runProjection() {
    docker-php-await ${DB_HOST}:${DB_PORT} -- artisan event-store:projection:run "$@"
}

function runProjectionNew() {
    docker-php-await ${DB_HOST}:${DB_PORT} -- artisan event-store:projection-new:run "$@"
}

function runCommand() {
    artisan "$@"
}

function runSchedule() {
    crond -f
}

function workQueue() {
    docker-php-await ${DB_HOST}:${DB_PORT} -- artisan queue:work --verbose "$@"
}

function testApplication() {
    if [[ -x "$(command -v phpunit)" ]]; then
        phpunit -v "$@"
        exit $?;
    else
        echo "This command is for use in environments build for development. PHPUnit is not installed."
        exit 1;
    fi
}

function showUsage() {
    echo "Intelligence Fusion 2 Application Server"
    echo ""
    echo "Usage: fusion [OPTIONS] COMMAND [ARGS...]"
    echo ""
    echo "Options:"
    echo "  -h|--help               Prints this usage information"
    echo ""
    echo "Commands:"
    echo "  health-check              Runs the health check process against this container. Make sure 'serve' has been called first."
    echo "  composer                  Run composer command to install/update dependencies"
    echo "  run <command>             Run a CLI command within the application. This currently goes through Artisan."
    echo "  project <projection>      Start the given projector (long running)."
    echo "  project-new <projection>  Start the given projector (long running). Projections using this command implement the new Projector interface"
    echo "  schedule                  Start the task scheduler (long running)."
    echo "  serve                     Start the FPM application server (long running)."
    echo "  test                      Run automated tests. This will not work in production builds."
    echo "  work                      Work through the job queue (usually long running)."
}

# If fewer than one argument supplied, show usage
if [[  $# -le 0 ]]
then
    showUsage
    exit 1
fi

# Now check for the command that was asked for and try to run it
case "$1" in
"-h|--help")
    showUsage
    exit 0
    ;;
"composer")
    shift
    runComposer "$@"
    exit 0
    ;;
"health-check")
    shift
    healthCheckApplication
    exit 0
    ;;
"migrate")
    shift
    runMigrate
    exit 0
    ;;
"run")
    shift
    runCommand "$@"
    exit 0
    ;;
"project")
    shift
    runProjection "$@"
    ;;
"project-new")
    shift
    runProjectionNew "$@"
    ;;
"schedule")
    shift
    runSchedule
    ;;
"serve")
    shift
    serveApplication "$@"
    ;;
"test")
    shift
    testApplication "$@"
    exit 0
    ;;
"work")
    shift
    workQueue "$@"
    ;;
*)
    echo "Unknown command '$1'. Use --help to show usage instructions."
    exit 127
esac
