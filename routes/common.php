<?php

use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Router;

/** @var Router $router */

$router->get("/", function(){
    return new Response('Good luck - we believe in you!!');
});
