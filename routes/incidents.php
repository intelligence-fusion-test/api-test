<?php

use Laravel\Lumen\Routing\Router;

/** @var Router $router */

$router->group(['prefix' => 'incidents'], static function() use ($router) {
    $router->post("/", 'IncidentsController@store');
    $router->get("{incidentId}", 'IncidentsController@show');
});
