<?php

namespace Fusion\Incidents\Domain\Entity;

use Fusion\Common\Application\Exception\NotFoundException;
use Fusion\Incidents\Domain\ValueObject\IncidentId;
use Fusion\Incidents\Domain\ValueObject\Map\Geometry\Position;

class Incident
{
    /**
     * @var IncidentId
     */
    private $id;
    /**
     * @var string
     */
    private $description;
    /**
     * @var \DateTimeImmutable
     */
    private $reportedAt;
    /**
     * @var object
     */
    private $category;
    /**
     * @var Position
     */
    private $position;

    public function __construct(IncidentId $id, string $description, \DateTimeImmutable $reportedAt, object $category, $position)
    {
        $this->id = $id;
        $this->description = $description;
        $this->reportedAt = $reportedAt;
        $this->category = $category;
        $this->position = $position;
    }

    public function getId(): IncidentId
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function getReportedAt(): \DateTimeImmutable
    {
        return $this->reportedAt;
    }

    public function getCategory(): object
    {
        return $this->category;
    }

    public function getPosition(): Position
    {
        return $this->position;
    }

    public static function create(IncidentId $id, $data): self
    {
        return new self(
            $id,
            $data->description,
            \DateTimeImmutable::createFromFormat('U', $data->reportedAt),
            (object) [
                'id' => $data->categoryId,
                'name' => $data->categoryName,
                'colour' => $data->categoryColour
            ],
            Position::fromCoordinates($data->position->latitude, $data->position->longitude)
        );
    }

    public function save()
    {
        \DB::table('incidents')->insert([
            'id' => $this->id->toString(),
            'description' => $this->description,
            'reported_at' => $this->reportedAt->getTimestamp(),
            'category_id' => $this->category->id,
            'category_name' => $this->category->name,
            'category_colour' => $this->category->colour,
            'position' => $this->position->toString(),
        ]);
    }

    /**
     * @param IncidentId $id
     * @return static
     * @throws NotFoundException
     */
    public static function get(IncidentId $id): self
    {
        $incident = \DB::table('incidents')->find($id->toString());

        if ($incident === null) {
            throw new NotFoundException('Incident does not exist');
        }

        return new self(
            IncidentId::fromString($incident->id),
            $incident->description,
            \DateTimeImmutable::createFromFormat('U', $incident->reported_at),
            (object) [
                'id' => $incident->category_id,
                'name' => $incident->category_name,
                'colour' => $incident->category_colour
            ],
            Position::fromString($incident->position)
        );
    }
}
