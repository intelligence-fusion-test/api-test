<?php

namespace Fusion\Incidents\Domain\ValueObject\Map\Geometry;

use Fusion\Common\Domain\Model\Assert;
use Fusion\Common\Domain\Model\InvariantException;
use Fusion\Common\Domain\Model\ValueObject;

class Position extends ValueObject implements Geometry
{
    /**
     * @var float
     */
    private $latitude;
    /**
     * @var float
     */
    private $longitude;

    public static function atOrigin(): Position
    {
        return new self(0.0, 0.0);
    }

    public static function fromCoordinates(float $latitude, float $longitude): Position
    {
        return new self(
            round($latitude, 6),
            round($longitude, 6)
        );
    }

    public static function fromString(string $pointString): Position
    {
        Assert::that($pointString)->regex('/^POINT\s\(.+\)/', 'The line string is not in a valid format');

        preg_match_all('/\([\-\d\.\s]+\)/', $pointString, $pointStringMatches);

        $coordinateString = rtrim(ltrim($pointStringMatches[0][0], '('), ')');
        $coordinates = explode(" ", $coordinateString);

        return new self(
            (float) $coordinates[1],
            (float) $coordinates[0]
        );
    }

    private function __construct(float $latitude, float $longitude)
    {
        if ($latitude > 90.0 || $latitude < -90.0) {
            throw new InvariantException('Latitude must be between 90 and -90');
        }

        if ($longitude > 180.0 || $longitude < -180.0) {
            throw new InvariantException('Longitude must be between 180 and -180');
        }

        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    public function withAdjustedCoordinates(array $coordinates): Geometry
    {
        return new self(
            (float) $coordinates[1],
            (float) $coordinates[0]
        );
    }

    /**
     * Get the latitude (Y) co-ordinate
     *
     * @return float
     */
    public function latitude(): float
    {
        return $this->latitude;
    }

    /**
     * Get the longitude (X) co-ordinate
     *
     * @return float
     */
    public function longitude(): float
    {
        return $this->longitude;
    }

    public function type(): string
    {
        return 'Point';
    }

    /**
     * @return array|float[]
     */
    public function coordinates(): array
    {
        return [$this->longitude, $this->latitude];
    }

    public function __toString(): string
    {
        $longitude = number_format($this->longitude, 6);
        $latitude = number_format($this->latitude, 6);

        return sprintf("POINT (%s %s)", $longitude, $latitude);
    }
}
