<?php

namespace Fusion\Incidents\Domain\ValueObject\Map\Geometry;

interface Geometry
{
    public function withAdjustedCoordinates(array $coordinates): Geometry;

    public function toString(): string;

    /**
     * Get the simple type name for this geometry, e.g. Polygon, Point
     *
     * @return string
     */
    public function type(): string;

    public function coordinates(): array;
}
