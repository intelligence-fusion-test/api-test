<?php

namespace Fusion\Incidents\Application\Command;

use Fusion\Incidents\Domain\Entity\Incident;

class CreateIncidentHandler
{
    public function __invoke(CreateIncidentCommand $command)
    {
        Incident::create($command->getIncidentId(), $command->getData())->save();
    }
}
