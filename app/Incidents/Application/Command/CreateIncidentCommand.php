<?php

namespace Fusion\Incidents\Application\Command;

use Fusion\Incidents\Domain\ValueObject\IncidentId;

class CreateIncidentCommand
{
    /**
     * @var object
     */
    private $data;
    /**
     * @var IncidentId
     */
    private $incidentId;

    public function __construct(object $data)
    {
        $this->data = $data;
        $this->incidentId = IncidentId::generate();
    }

    public function getData(): object
    {
        return $this->data;
    }

    public function getIncidentId(): IncidentId
    {
        return $this->incidentId;
    }
}
