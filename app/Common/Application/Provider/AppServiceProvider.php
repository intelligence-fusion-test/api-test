<?php

namespace Fusion\Common\Application\Provider;

use Fusion\Common\Application\LumenContainer;
use Illuminate\Cache\CacheManager;
use Illuminate\Config\Repository as ConfigRepository;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Filesystem\FilesystemManager;
use Illuminate\Log\LogManager;
use Illuminate\Mail\MailServiceProvider;
use Illuminate\Support\ServiceProvider;
use Prooph\Common\Event\ActionEventEmitter;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\MessageBus;
use Prooph\ServiceBus\Plugin\ServiceLocatorPlugin;
use Prooph\ServiceBus\QueryBus;

class AppServiceProvider extends ServiceProvider
{
    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        CacheManager::class => 'cache',
        LogManager::class => 'log',
        ConfigRepository::class => 'config',
        FilesystemManager::class => 'filesystem',
        Mailer::class => 'mailer',
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        // Bindings, singletons and aliases
        $this->registerBindings();

        $this->registerMessageBuses();

        // Mail and other Integrations
        $this->app->register(MailServiceProvider::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        // Ensure all config files are loaded and applied
        if ($this->app instanceof \Laravel\Lumen\Application) {
            $this->app->configure('app');
            $this->app->configure('auth');
            $this->app->configure('broadcasting');
            $this->app->configure('cache');
            $this->app->configure('database');
            $this->app->configure('logging');
            $this->app->configure('mail');
            $this->app->configure('queue');
            $this->app->configure('view');
        }
    }

    /**
     * Registers the command bus and query bus with the application
     */
    private function registerMessageBuses(): void
    {
        $container = new LumenContainer($this->app);
        $serviceLocator = new ServiceLocatorPlugin($container);

        // Bind action event emitter so that it is available for decoration
        $this->app->bind(ActionEventEmitter::class, function () {
            $bus = new class extends MessageBus{
                public function dispatch($message)
                {
                }

                public function getEmitter()
                {
                    return $this->events;
                }
            };

            return $bus->getEmitter(); // default emitter from a message bus
        });

        // Command Bus
        $this->app->singleton(CommandBus::class, function (Container $app) use ($serviceLocator): CommandBus {
            $commandBus = new CommandBus($this->app->make(ActionEventEmitter::class));

            $serviceLocator->attachToMessageBus($commandBus);

            return $commandBus;
        });

        // Query Bus
        $this->app->singleton(QueryBus::class, function () use ($serviceLocator): QueryBus {
            $queryBus = new QueryBus($this->app->make(ActionEventEmitter::class));

            // Attach plugins
            $serviceLocator->attachToMessageBus($queryBus);

            return $queryBus;
        });
    }

    /**
     * Register regular bindings and singletons
     */
    private function registerBindings(): void
    {
        // Register standard bindings
        foreach ($this->bindings as $abstract => $concrete) {
            $this->app->bind($abstract, $concrete);
        }
    }
}
