<?php

namespace Fusion\Common\Application\Exception;

class ValidationException extends \Exception
{
    public static function fromMessages(array $messages): self
    {
        return new self(json_encode($messages));
    }

    public function getMessageArray(): array
    {
        if (!$this->message) {
            return [];
        }

        return json_decode($this->message, true);
    }
}
