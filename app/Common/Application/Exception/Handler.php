<?php

namespace Fusion\Common\Application\Exception;

use ErrorException;
use Exception;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Prooph\ServiceBus\Plugin\Guard\UnauthorizedException;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    public function render($request, Exception $e)
    {
        // Extract message bus exceptions before rendering
        if ($e instanceof CommandDispatchException || $e instanceof MessageDispatchException) {
            $e = $this->extractMessageException($e);
        }

        // Return JSON if requested, or by default
        if ($request->acceptsJson()) {
            return $this->renderJson($request, $e);
        }

        // Otherwise, return HTML if requested
        if ($request->acceptsHtml()) {
            return $this->renderHtml($request, $e);
        }

        // Can't return an acceptable content type
        return response('', Response::HTTP_NOT_ACCEPTABLE);
    }

    private function renderJson(Request $request, Exception $ex): JsonResponse
    {
        // Return pre-made responses
        if ($ex instanceof HttpResponseException || $ex instanceof ValidationException) {
            return $ex->getResponse();
        }

        // Set defaults
        $status = Response::HTTP_INTERNAL_SERVER_ERROR;
        $message = "A problem occurred while trying to handle your request. Please try again later.";
        $exception = $ex;
        $headers = [];

        // Handle specific problems
        if ($ex instanceof AuthenticationException) {
            $status = Response::HTTP_UNAUTHORIZED;
            $message = 'You must be logged-in to do that';
            $exception = new HttpException($status, $message);
        } elseif ($ex instanceof AuthorizationException || $ex instanceof UnauthorizedException) {
            $status = Response::HTTP_FORBIDDEN;
            $message = $ex->getMessage() ?: 'You do not have permission to do that';
            $exception = new HttpException($status, $message);
        } elseif ($ex instanceof HttpException) {
            $status = $ex->getStatusCode();
            $message = $ex->getMessage();
            $headers = $ex->getHeaders();
        } elseif ($ex instanceof ClientException) {
            $status = Response::HTTP_SERVICE_UNAVAILABLE;
            $message = 'The user service is not currently available. Please try again in a few moments.';
        }

        // Prepare the response body
        $body = compact('status', 'message');

        // Add debug information as required
        if (env('APP_DEBUG')) {
            $body['method'] = $request->method();
            $body['url'] = $request->fullUrl();
            $body['exceptions'] = FlattenException::createFromThrowable($exception)->toArray();
        }

        return response()->json($body, $status, $headers);
    }

    private function renderHtml(Request $request, Exception $ex): Response
    {
        return parent::render($request, $ex);
    }

    private function extractMessageException(MessageDispatchException $exception): Exception
    {
        $exception = $exception->getPrevious();

        if (!$exception instanceof Exception) {
            $exception = new ErrorException(
                $exception->getMessage(),
                $exception->getCode(),
                1,
                $exception->getFile(),
                $exception->getLine(),
                $exception->getPrevious()
            );
        }

        return $exception;
    }
}
