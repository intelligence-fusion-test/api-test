<?php

namespace Fusion\Common\Application\Validation;

use Fusion\Common\Application\Exception\ValidationException;
use Illuminate\Contracts\Validation\Factory;

class PropertyValidator
{
    /**
     * @var Factory
     */
    private $factory;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param array $props
     * @param array $rules
     * @param array $messages
     * @param array $customAttributes
     * @throws ValidationException
     */
    public function validateArrayProperties(
        array $props,
        array $rules,
        array $messages = [],
        array $customAttributes = []
    ): void {
        $validator = $this->factory->make($props, $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            throw ValidationException::fromMessages($validator->errors()->messages());
        }
    }
}
