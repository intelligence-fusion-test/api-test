<?php

namespace Fusion\Common\Application\Job;

use Fusion\Common\Application\Query\DispatchesQueries;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

abstract class Job implements ShouldQueue
{
    /*
    |--------------------------------------------------------------------------
    | Queueable Jobs
    |--------------------------------------------------------------------------
    |
    | This job base class provides a central location to place any logic that
    | is shared across all of your jobs. The trait included with the class
    | provides access to the "queueOn" and "delay" queue helper methods.
    |
    */
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;
    use DispatchesQueries;

    protected $meta = [];

    /**
     * Retry this job by releasing it back onto the queue. The delay factor is used to make the delay proportional to
     * the number of attempts already made.
     *
     * @param int $delayFactor
     */
    protected function retry(int $delayFactor = 5): void
    {
        $delaySeconds = ($this->attempts() - 1) * $delayFactor;

        $this->release($delaySeconds);
    }
}
