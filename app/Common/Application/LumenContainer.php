<?php

namespace Fusion\Common\Application;

use Illuminate\Contracts\Container\Container;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use ReflectionException;

final class LumenContainer implements ContainerInterface
{
    /**
     * @var Container
     */
    private $container;
    /**
     * @var array
     */
    private $cacheForHas;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->cacheForHas = [];
    }

    public function get($id)
    {
        return $this->container->make($id);
    }

    public function has($id)
    {
        if ($this->hasIsCached($id)) {
            return $this->hasFromCache($id);
        }

        $has = $this->container->bound($id) || $this->isInstantiable($id);

        $this->cacheHas($id, $has);

        return $has;
    }

    private function hasIsCached(string $id): bool
    {
        return array_key_exists($id, $this->cacheForHas);
    }

    private function hasFromCache(string $id)
    {
        return $this->cacheForHas[$id];
    }

    private function cacheHas(string $id, bool $has)
    {
        $this->cacheForHas[$id] = $has;
    }

    private function isInstantiable(string $id): bool
    {
        if (class_exists($id)) {
            return true;
        }
        try {
            $reflectionClass = new ReflectionClass($id);

            return $reflectionClass->isInstantiable();
        } catch (ReflectionException $e) {
            return false;
        }
    }
}
