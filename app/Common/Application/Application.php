<?php

namespace Fusion\Common\Application;

use Laravel\Lumen\Application as LumenApplication;

final class Application extends LumenApplication
{
    public function isDownForMaintenance()
    {
        return filter_var(env('APP_MAINTENANCE', 'false'), FILTER_VALIDATE_BOOLEAN);
    }
}
