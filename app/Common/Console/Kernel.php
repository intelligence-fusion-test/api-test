<?php

namespace Fusion\Common\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     * @throws \Exception
     */
    protected function schedule(Schedule $schedule)
    {
        // Expired password resets
        $schedule->command('auth:clear-resets')
            ->everyMinute()
            ->evenInMaintenanceMode();

        // Expired task redirection
        $schedule->command('tasks:clear-redirections')
            ->everyMinute()
            ->evenInMaintenanceMode();

        $schedule->command('incidents:ongoing-prompt', ['days' => 5])
            ->hourly()
            ->evenInMaintenanceMode();

        // Trending theme calculation
        $schedule->command('trends:calculate')
            ->everyTenMinutes()
            ->evenInMaintenanceMode();

        // Fuzzy-search index
        $schedule->command('search:reindex')
            ->hourly()
            ->evenInMaintenanceMode();
    }
}
