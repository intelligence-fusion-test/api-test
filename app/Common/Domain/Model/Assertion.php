<?php

namespace Fusion\Common\Domain\Model;

use Assert\Assertion as BaseAssertion;

abstract class Assertion extends BaseAssertion
{
    protected static $exceptionClass = InvariantException::class;
}
