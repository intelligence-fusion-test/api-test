<?php

namespace Fusion\Common\Domain\Model;

use Assert\Assert as BaseAssert;

abstract class Assert extends BaseAssert
{
    protected static $assertionClass = Assertion::class;
}
